/**************************************************************************
  ISEN Controller

  The ISEN controller is responsible for the read out of the Accelerometer ,Gyroscope and Magnetometer sensor and sending this information through the Bluetooth Low Energy module to a device.
  It also is responsible for reading out the pushbutton and controlling the color of the onboard NeoPixel.

  The components:
  * Adafruit Flora V2:                        the main micro controller board for the project.
  * Adafruit 9-DOF LSM9DS0:                   the Accelerometer ,Gyroscope and Magnetometer sensor. Connected to the Flora main board on the pins: 3,3 V, GND, SDA and SCL.
  * Adafruit Flora Bluefruit LE:              the Bluetooth Low Energy module.                      Connected to the Flora main board on the pins: 3,3 V, GND, RX  and TX.
  * Adafruit FSR (Force Sensitive Resistor):  the pushbutton for the project.                       Connected to the Flora main board on the pins: D6 and GND.
  * Adafruit Battery Holder 3xAAA:            the power supply for the project.                     Connected to the Flora main board on the JST port.

**************************************************************************/


//Libraries for the Accelerometer ,Gyroscope and Magnetometer sensor.
#include <Adafruit_LSM9DS0.h>

//Declaring the Accelerometer ,Gyro and Magnetometer sensor variable.
Adafruit_LSM9DS0 agmSensor = Adafruit_LSM9DS0();

//Declaring a sensor event for the accelerometer ,gyroscope and magnetometer, the data from the sensor will be written to these variables.
sensors_event_t accelerometer, magnetometer, gyroscope, thermometer;

//Libraries for the Attitude and Heading Reference System.
#include <Adafruit_Simple_AHRS.h>

//Declaring the Attitude and Heading Reference System variable.
Adafruit_Simple_AHRS ahrSystem(&agmSensor.getAccel(), &agmSensor.getMag());

//Declaring a sensor vector for the orientation, the data from the Attitude and Heading Reference System will be written to this variable.
sensors_vec_t orientation;

//Libraries for the Bluetooth LE module.
#include <Adafruit_BLE.h>
#include <Adafruit_BluefruitLE_UART.h>

//Declaring the Bluetooth LE module variable, declaring Serial1 as the name for the serial connection and sets the mode pin to -1.
Adafruit_BluefruitLE_UART bluetoothLEmodule(Serial1, -1);

//Libraries for the Onboard NeoPixel.
#include <Adafruit_NeoPixel.h>

//Declaring the Onboard NeoPixel variable, declaring 1 NeoPixel on pin 8 of the type NEO_GRB + NEO_KHZ800.
Adafruit_NeoPixel neopixel = Adafruit_NeoPixel(1, 8, NEO_GRB + NEO_KHZ800);

//Setting the pin for the pushButton.
int pushbuttonpin = 6;

//Declaring a OnOffSwitch, this will help to turn the pushButton in to a On Off Switch.
boolean onOffSwitch = false;

/*Declaring the variables needed to calculate the current velocity.
With the old time, the old acceleration, a new time and the new acceleration the velocity can be calculated.
The boolean is needed to know if it's the first run or not, because on the first run the velocity can't be calculated.*/
float timeOld = 0;
float accelerationOld[3] = {0,0,0};
boolean firstRun = true;

//Declaring the max velocity, this way it holds the value in it's memory.
float velocityMax = 0;

//This void runs first and only once, here the basic setup for the board is done.
void setup(void)
{
  
  //Set up NeoPixel for use, sets the NeoPixel in the begin state with a given brightness and pushes the result with show.
  neopixel.begin();
  //neopixel.setBrightness(10);
  neopixel.show();

  //Sets the pin for the pushButton as an input.
  pinMode(pushbuttonpin, INPUT_PULLUP);

  //Checks if the Accelerometer ,Gyro and Magnetometer sensor is available and the Bluetooth LE module is available.
  if (!agmSensor.begin() || !bluetoothLEmodule.begin())
  {
    //If there is a problem with the Accelerometer ,Gyro and Magnetometer sensor or Bluetooth LE module an error is given.
    deviceError();
  }

  //Setting sensitivity of the Accelerometer ,Gyro and Magnetometer
  agmSensor.setupAccel(agmSensor.LSM9DS0_ACCELRANGE_2G);
  agmSensor.setupMag(agmSensor.LSM9DS0_MAGGAIN_2GAUSS);
  agmSensor.setupGyro(agmSensor.LSM9DS0_GYROSCALE_245DPS);
  
  //Turns off the mode led on the Bluetooth LE module, this isn't necessary for this project.
  bluetoothLEmodule.sendCommandCheckOK("AT+HWModeLED= DISABLE");

  //Disables the command echo from Bluetooth LE module, this is necessary to be able to send information through the serial connection of the Bluetooth LE module.
  bluetoothLEmodule.echo(false);
}

//The main loop, this runs repeatedly.
void loop()
{
  //Checks if a device is connected to the Bluetooth LE module
  checkConnection();

  //Checks if the pushbutton is pressed and turns it in to a On Off Switch
  if(digitalRead(pushbuttonpin) == LOW && onOffSwitch == false)
  {
    onOffSwitch = true;
    firstRun = true;
    opnieuw = true;
    velocityMax = 0;
  }
  else if(digitalRead(pushbuttonpin) == LOW && onOffSwitch == true)
  {
    onOffSwitch = false;
    bluetoothLEmodule.println("AT+BLEUARTTX=save");
    bluetoothLEmodule.waitForOK();
  }
  
  //When the On Off Switch is on, the data from the sensor will be send trough the serial connection of the Bluetooth LE module.
  if(onOffSwitch)//count < 10)
  {
    //The NeoPixel is set to green, to indicate the sending of information.
    setNeoPixelColor(0, 255, 0);

    //Calculating the new velocity.
    float newVelocity = calculateNewVelocity();
    //Serial.println("Speed Now: " + (String)newVelocity);
    //Serial.println("Speed Max: " + (String)calculateMaxVelocity(newVelocity));


    
    //The sensor information is send through the serial connection of the Bluetooth LE module.
    bluetoothLEmodule.println("AT+BLEUARTTX=" + (String)newVelocity + "," + (String)calculateMaxVelocity(newVelocity));
    bluetoothLEmodule.waitForOK();
    /*bluetoothLEmodule.println("AT+BLEUARTTX=" +
                             (String)accelerometer.acceleration.x + ", " +
                             (String)accelerometer.acceleration.y + ", " +
                             (String)accelerometer.acceleration.z + ", " +
                             (String)magnetometer.magnetic.x + ", " +
                             (String)magnetometer.magnetic.y + ", " +
                             (String)magnetometer.magnetic.z + ", " +
                             (String)gyroscope.gyro.x + ", " +
                             (String)gyroscope.gyro.y + ", " +
                             (String)gyroscope.gyro.z + ", " +
                             (String)thermometer.temperature);*/
  }
  else
  {
    //When there is a device connection, the NeoPixel color will be set to blue to indicate this.
    setNeoPixelColor(0, 0, 255);
  }
  //The delay function is called, to pause the program for a few milliseconds.
  delay(250);
}

//If something goes wrong in the basic setup, this method will handle the error.
void deviceError()
{
  //If an error is found with the device, the NeoPixel will blink red to indicate this error.
  while(true)
  {
    //Set color to red.
    setNeoPixelColor(255, 0, 0);
    //Delay to get a blink effect.
    delay(500);
    //Set color to zero, to turn off the NeoPixel.
    setNeoPixelColor(0, 0, 0);
    //Delay to get a blink effect.
    delay(500);
  }
}

//This method checks if a device is connected to the Bluetooth LE module.
void checkConnection()
{
  //While there is no device connection, the NeoPixel color will be set to white to indicate that the device is ready but doesn't have a connection.
  //Sets the On Off Switch to false, to make sure there is no data send after the connection established but the button is not yet pushed.
  while(!bluetoothLEmodule.isConnected())
  {
    setNeoPixelColor(255, 255, 255);
    onOffSwitch = false;
  }
}

//Sets the color for the onboard NeoPixel.
void setNeoPixelColor(int red, int green, int blue)
{
  //Sets the color on the NeoPixel at position 0 with a red, green and blue color and pushes the result with show.
  neopixel.setPixelColor(0, red, green, blue);
  neopixel.show();
}

//Calculates the new velocity, with the data of the accelerometer.
float calculateNewVelocity()
{
    //Information from the sensor is retrieved.
    agmSensor.getEvent(&accelerometer, &magnetometer, &gyroscope, &thermometer);
    float accelerationNew[3] = {accelerometer.acceleration.x,
                                accelerometer.acceleration.y,
                                accelerometer.acceleration.z};

    float timeNew = accelerometer.timestamp;

    //Declaring the variable to store the new velocity in.
    float velocityNew = 0;
    
    //On the first run, the velocity cannot be calculated because there is not yet enough information.
    if(!firstRun)
    {
      /*The average velocity is calculated by taking the square root of the squares of the x, y and z data of the accelerometer.
        Then the average velocity is multiplied by the time to get the velocity in m/s.
        The time is calculated by subtracting the new from the old time to get the time of the acceleration. Because the time is in milliseconds, it is divided by 1000 to get the time in seconds.
        Finally the velocity in m/s is multiplied by 3,6 to get the velocity in km/h.*/
      velocityNew = sqrt( sq(accelerationOld[0] - accelerationNew[0]) +
                          sq(accelerationOld[1] - accelerationNew[1]) +
                          sq(accelerationOld[2] - accelerationNew[2])) *
                    (timeNew - timeOld) /1000
                    * 3.6f;      
    }
    else
    {
      //After the first run the boolean is set to false.
      firstRun = false;
    }

    //Saving the new time as the old time, this is necessary on the next run to calculated the velocity.
    timeOld = timeNew;

    //Saving the new acceleration as the old acceleration, this is necessary on the next run to calculated the velocity.
    accelerationOld[0] = accelerationNew[0]; 
    accelerationOld[1] = accelerationNew[1]; 
    accelerationOld[2] = accelerationNew[2]; 
    
    //Returning the value of the new velocity
    return velocityNew;
}

//Calculates the max velocity, in comparison to the new velocity.
float calculateMaxVelocity(float velocityNew)
{
  //It checks if the given new velocity is greater than the max velocity in memory. If it is true, then there is a new max velocity and the value of the new velocity is written to the max velocity.
  if(velocityNew > velocityMax)
  {
    velocityMax = velocityNew;
  }
  //Returning the value of the max velocity
  return velocityMax;
}

